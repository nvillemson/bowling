package general;

import com.project.Application;
import com.project.persistence.repository.BaseTestRepository;
import com.project.web.pojo.request.AddGameRequest;
import com.project.web.pojo.request.AddPlayerRequest;
import com.project.web.pojo.request.MakeShotRequest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public abstract class AbstractTest {

    protected final String addGameUrl = "/add-game";
    protected final String startGameUrl = "/start-game";
    protected final String addPlayerUrl = "/add-player";
    protected final String makeShotUrl = "/make-shot";
    protected final String getPlayerTurnUrl = "/get-player-turn";
    protected final String getPlayerPointsUrl = "/get-player-points";
    protected final String getAllPointsUrl = "/get-all-points";
    protected final String getPlayerTableUrl = "/get-player-table";

    @Value("${server.port}")
    private int serverPort;

    @Autowired
    private BaseTestRepository baseTestRepository;

    @Before
    public void setUp() {
        baseTestRepository.clearDatabase();
        RestAssured.port = serverPort;
    }

    protected long addGame(AddGameRequest request){
        return given()
                .body(request)
                .contentType(ContentType.JSON)
                .when()
                .post(addGameUrl)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response().jsonPath().getLong("id");
    }

    protected void startGame(long gameId){
        given()
                .post(startGameUrl + "?gameId=" + gameId)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    protected long addPlayer(AddPlayerRequest request){
        return given()
                .body(request)
                .contentType(ContentType.JSON)
                .when()
                .post(addPlayerUrl)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response().jsonPath().getLong("id");
    }

    protected void makeShot(MakeShotRequest request){
        given()
                .body(request)
                .contentType(ContentType.JSON)
                .when()
                .post(makeShotUrl)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    protected Response getPlayerTurn(long gameId){
        return given()
                .get(getPlayerTurnUrl + "?gameId=" + gameId)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response();
    }

    protected void makeShotAndAssertNext(long gameId, long playerId, long nextPlayer, int score, int frame) {
        makeShot(new MakeShotRequest(gameId, playerId, score, frame));
        long nextActualPlayer = getPlayerTurn(gameId).jsonPath().getLong("playerId");
        assertEquals(nextPlayer, nextActualPlayer);
    }
}
