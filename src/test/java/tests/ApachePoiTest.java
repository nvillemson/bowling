package tests;

import general.AbstractTest;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ApachePoiTest extends AbstractTest {

    @Test
    public void writeExcel() throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("Excel test for fun.");

        CellStyle headerStyle = getHeaderFont(workbook);

        Row headerRow = sheet.createRow(0);
        String[] header = new String[] {"FIRST", "SECOND", "THIRD"};
        for (int i = 0; i < header.length; i++) {
            Cell headerCell = headerRow.createCell(i);
            headerCell.setCellStyle(headerStyle);
            headerCell.setCellValue(header[i]);
        }

        String[][] data = new String[][] {
                {"1", "1A", "1B"},
                {"2", "2A", "2B"},
                {"3", "3A", "3B"},
        };

        for (int i = 0; i < data.length; i++) {
            Row dataRow = sheet.createRow(i + 1);
            for (int j = 0; j < data[i].length; j++) {
                Cell dataCell = dataRow.createCell(j);
                dataCell.setCellValue(data[i][j]);
            }
        }

        FileOutputStream fos =new FileOutputStream(new File("C://testExcel/my.xlsx"));
        workbook.write(fos);
        fos.close();
        System.out.println("Done");
    }

    private CellStyle getHeaderFont(Workbook workbook) {
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        headerCellStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
        headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        headerCellStyle.setBorderBottom(BorderStyle.THICK);
        headerCellStyle.setBorderLeft(BorderStyle.THICK);
        headerCellStyle.setBorderRight(BorderStyle.THICK);
        headerCellStyle.setBorderTop(BorderStyle.THICK);

        headerCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        headerCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        headerCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        headerCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

        return headerCellStyle;
    }
}
