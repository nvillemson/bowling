package tests;

import com.project.web.pojo.request.AddGameRequest;
import com.project.web.pojo.request.AddPlayerRequest;
import general.AbstractTest;
import org.apache.http.HttpStatus;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class GameStatisticsControllerTest extends AbstractTest {

    @Test
    public void testGameStats() {
        long gameId = addGame(new AddGameRequest("Game 1"));
        long playerId1 = addPlayer(new AddPlayerRequest("Player 1", gameId));
        long playerId2 = addPlayer(new AddPlayerRequest("Player 2", gameId));
        startGame(gameId);

        // p1 = 8
        makeShotAndAssertNext(gameId, playerId1, playerId1, 7, 1);
        makeShotAndAssertNext(gameId, playerId1, playerId2, 1, 1);
        // p2 = 0
        makeShotAndAssertNext(gameId, playerId2, playerId2, 0, 1);
        makeShotAndAssertNext(gameId, playerId2, playerId1, 0, 1);

        // p1 = 14
        makeShotAndAssertNext(gameId, playerId1, playerId1, 5, 2);
        makeShotAndAssertNext(gameId, playerId1, playerId2, 1, 2);
        // p2 = 27
        makeShotAndAssertNext(gameId, playerId2, playerId2, 10, 2);
        makeShotAndAssertNext(gameId, playerId2, playerId2, 10, 2);
        makeShotAndAssertNext(gameId, playerId2, playerId1, 7, 2);

        given()
                .get(getPlayerPointsUrl + "?gameId=" + gameId + "&playerId=" + playerId1)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("playerId", equalTo((int) playerId1))
                .body("playerName", equalTo("Player 1"))
                .body("playerPoints", equalTo(14));

        given()
                .get(getAllPointsUrl + "?gameId=" + gameId)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("response.size()", equalTo(2))
                .body("response[0].playerId", equalTo((int) playerId1))
                .body("response[0].playerName", equalTo("Player 1"))
                .body("response[0].playerPoints", equalTo(14))
                .body("response[1].playerId", equalTo((int) playerId2))
                .body("response[1].playerName", equalTo("Player 2"))
                .body("response[1].playerPoints", equalTo(27));

        given()
                .get(getPlayerTableUrl + "?gameId=" + gameId + "&playerId=" + playerId2)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("playerShots.size()", equalTo(5))
                .body("playerShots[0].playerId", equalTo((int) playerId2))
                .body("playerShots[0].score", equalTo(0))
                .body("playerShots[0].scoreNumber", equalTo(1))
                .body("playerShots[1].playerId", equalTo((int) playerId2))
                .body("playerShots[1].score", equalTo(0))
                .body("playerShots[1].scoreNumber", equalTo(2))
                .body("playerShots[2].playerId", equalTo((int) playerId2))
                .body("playerShots[2].score", equalTo(10))
                .body("playerShots[2].scoreNumber", equalTo(1))
                .body("playerShots[3].playerId", equalTo((int) playerId2))
                .body("playerShots[3].score", equalTo(10))
                .body("playerShots[3].scoreNumber", equalTo(2))
                .body("playerShots[4].playerId", equalTo((int) playerId2))
                .body("playerShots[4].score", equalTo(7))
                .body("playerShots[4].scoreNumber", equalTo(3));
    }
}
