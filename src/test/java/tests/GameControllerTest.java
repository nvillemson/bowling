package tests;

import com.project.web.pojo.request.AddGameRequest;
import com.project.web.pojo.request.AddPlayerRequest;
import com.project.web.pojo.request.MakeShotRequest;
import general.AbstractTest;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

public class GameControllerTest extends AbstractTest {

    @Test
    public void testAddPlayerNoGameBadRequest() {
        given()
                .body(new AddPlayerRequest("Tester", 1L))
                .contentType(ContentType.JSON)
                .when()
                .post(addGameUrl)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testAddPlayer() {
        long game1Id = addGame(new AddGameRequest("Game 1"));
        addPlayer(new AddPlayerRequest("Player 1", game1Id));
        addPlayer(new AddPlayerRequest("Player 2", game1Id));

        startGame(game1Id);

        given()
                .body(new AddPlayerRequest("Player 3", game1Id))
                .contentType(ContentType.JSON)
                .when()
                .post(addPlayerUrl)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testMakeShotBadResponse() {
        long game1Id = addGame(new AddGameRequest("Game 1"));
        long playerId1 = addPlayer(new AddPlayerRequest("Player 1", game1Id));
        long playerId2 = addPlayer(new AddPlayerRequest("Player 2", game1Id));
        startGame(game1Id);

        // no player found
        given()
                .body(new MakeShotRequest(game1Id, 100L, 7, 1))
                .contentType(ContentType.JSON)
                .when()
                .post(makeShotUrl)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);

        // wrong score
        given()
                .body(new MakeShotRequest(game1Id, playerId1, 17, 1))
                .contentType(ContentType.JSON)
                .when()
                .post(makeShotUrl)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);

        makeShot(new MakeShotRequest(game1Id, playerId1, 7, 1));

        // wrong score
        given()
                .body(new MakeShotRequest(game1Id, playerId1, 5, 1))
                .contentType(ContentType.JSON)
                .when()
                .post(makeShotUrl)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);

        makeShot(new MakeShotRequest(game1Id, playerId1, 1, 1));

        // player already had 2 shots
        given()
                .body(new MakeShotRequest(game1Id, playerId1, 1, 1))
                .contentType(ContentType.JSON)
                .when()
                .post(makeShotUrl)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testGame() {
        long gameId = addGame(new AddGameRequest("Game 1"));
        long playerId1 = addPlayer(new AddPlayerRequest("Player 1", gameId));
        long playerId2 = addPlayer(new AddPlayerRequest("Player 2", gameId));
        startGame(gameId);

        makeShotAndAssertNext(gameId, playerId1, playerId1, 3, 1);
        // score
        makeShotAndAssertNext(gameId, playerId1, playerId1, 7, 1);
        // p1 = 17
        makeShotAndAssertNext(gameId, playerId1, playerId2, 7, 1);

        // strike and p2 = 27
        makeShotAndAssertNext(gameId, playerId2, playerId2, 10, 1);
        makeShotAndAssertNext(gameId, playerId2, playerId2, 10, 1);

        makeShot(new MakeShotRequest(gameId, playerId2, 7, 1));
        long nextActualPlayer = getPlayerTurn(gameId).jsonPath().getLong("playerId");
        int currentFrame = getPlayerTurn(gameId).jsonPath().getInt("frame");
        assertEquals(playerId1, nextActualPlayer);
        assertEquals(currentFrame, 2);

        // p1 = 26
        makeShotAndAssertNext(gameId, playerId1, playerId1, 7, 2);
        makeShotAndAssertNext(gameId, playerId1, playerId2, 1, 2);
        // p2 = 27
        makeShotAndAssertNext(gameId, playerId2, playerId2, 0, 2);
        makeShotAndAssertNext(gameId, playerId2, playerId1, 0, 2);
    }
}
