package com.project.persistence.repository;

import com.project.web.pojo.helper.FrameResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Repository
public class GameRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public GameRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Transactional
    public Boolean checkGameExistsAndNotStarted(Long gameId) {
        return jdbcTemplate.queryForObject("SELECT EXISTS(SELECT ID FROM GAME WHERE ID  = ? AND START IS NULL)",
                new Object[]{ gameId }, Boolean.class);
    }

    @Transactional
    public Boolean checkGameExists(Long gameId) {
        return jdbcTemplate.queryForObject("SELECT EXISTS(SELECT ID FROM GAME WHERE ID  = ? )",
                new Object[]{ gameId }, Boolean.class);
    }

    @Transactional
    public Boolean checkGameExistsAndNotEnded(Long gameId) {
        return jdbcTemplate.queryForObject("SELECT EXISTS(SELECT ID FROM GAME WHERE ID  = ? AND END IS NULL AND START IS NOT NULL)",
                new Object[]{ gameId }, Boolean.class);
    }

    @Transactional
    public void startGame(long gameId) {
        jdbcTemplate.update("UPDATE GAME SET START = NOW() WHERE ID = ? AND START IS NULL", gameId);
    }

    @Transactional
    public Boolean checkPlayerRegisteredForGame(Long gameId, Long playerId) {
        return jdbcTemplate.queryForObject("SELECT EXISTS(SELECT 1 FROM GAME g INNER JOIN PLAYER p ON (p.GAME_ID = g.ID AND g.ID  = ? " +
                        "AND p.ID = ? ) WHERE g.END IS NULL AND g.START IS NOT NULL)",
                new Object[]{ gameId, playerId }, Boolean.class);
    }

    @Transactional
    public Integer getCurrentFrame(Long gameId) {
        return jdbcTemplate.queryForObject("SELECT MAX(s.FRAME) FROM GAME g INNER JOIN PLAYER p ON p.GAME_ID = g.ID " +
                        "LEFT JOIN SHOT s ON s.PLAYER_ID = p.ID " +
                        "WHERE g.ID = ? AND g.END IS NULL",
                new Object[]{ gameId }, Integer.class);
    }

    @Transactional
    public List<FrameResult> getFrameResults(Long gameId, int currentFrame) {
        return jdbcTemplate.query("SELECT p.ID AS playerId, s.SHOT_NUMBER AS shotNumber, s.SCORE AS shotScore " +
                        "FROM GAME g INNER JOIN PLAYER p ON (p.GAME_ID = g.ID  AND g.ID = ? ) " +
                        "LEFT JOIN SHOT s ON (s.PLAYER_ID = p.ID AND s.FRAME = ? ) " +
                        "WHERE g.END IS NULL " +
                        "ORDER BY p.ID ",
                new Object[]{ gameId, currentFrame }, (rs, rowNum) -> new FrameResult(rs.getLong("playerId"),
                        rs.getInt("shotNumber"), rs.getInt("shotScore")));
    }

    @Transactional
    public List<FrameResult> getFrameResultsForPlayer(Long gameId, int currentFrame, Long playerId) {
        return jdbcTemplate.query("SELECT p.ID AS playerId, s.SHOT_NUMBER AS shotNumber, s.SCORE AS shotScore " +
                        "FROM GAME g INNER JOIN PLAYER p ON (p.GAME_ID = g.ID) " +
                        "LEFT JOIN SHOT s ON s.PLAYER_ID = p.ID " +
                        "WHERE g.ID = ? AND g.END IS NULL AND s.FRAME = ? AND p.ID = ? " +
                        "ORDER BY p.ID ",
                new Object[]{ gameId, currentFrame, playerId }, (rs, rowNum) -> new FrameResult(rs.getLong("playerId"),
                        rs.getInt("shotNumber"), rs.getInt("shotScore")));
    }

    @Transactional
    public void endGame(Long gameId) {
        jdbcTemplate.update("UPDATE GAME SET END = NOW() WHERE ID = ?", gameId );
    }

    @Transactional
    public Long saveShot(Long playerId, Integer frame, Integer score, int shotNumber) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO SHOT(PLAYER_ID, FRAME, SCORE, SHOT_NUMBER, REGISTERED) " +
                    "VALUES (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            ps.setLong(1, playerId);
            ps.setInt(2, frame);
            ps.setInt(3, score);
            ps.setInt(4, shotNumber);
            ps.setTimestamp(5, Timestamp.valueOf(LocalDateTime.now()));
            return ps; }, holder);
        return Objects.requireNonNull(holder.getKey()).longValue();
    }

    @Transactional
    public Long addPlayer(String playerName, Long gameId) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO PLAYER(NAME, GAME_ID) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, playerName);
            ps.setLong(2, gameId);
            return ps; }, holder);
        return Objects.requireNonNull(holder.getKey()).longValue();
    }

    @Transactional
    public Long addGame(String gameName) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO GAME(NAME) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, gameName);
            return ps; }, holder);
        return Objects.requireNonNull(holder.getKey()).longValue();
    }
}
