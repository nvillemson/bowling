package com.project.persistence.repository;

import com.project.web.pojo.helper.PlayerTableEntry;
import com.project.web.pojo.response.AllPlayerPointsResponse;
import com.project.web.pojo.response.PlayerPointsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GameStatisticsRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public GameStatisticsRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public PlayerPointsResponse getPlayerPoints(long gameId, long playerId) {
        return jdbcTemplate.queryForObject("SELECT p.ID AS playerId, p.NAME AS playerName, SUM(s.SCORE) AS totalShotScore " +
                        "FROM GAME g INNER JOIN PLAYER p ON (p.GAME_ID = g.ID) " +
                        "LEFT JOIN SHOT s ON s.PLAYER_ID = p.ID " +
                        "WHERE g.ID = ? AND g.END IS NULL AND p.ID =? " +
                        "GROUP BY p.ID ",
                new Object[]{ gameId, playerId }, (rs, rowNum) -> new PlayerPointsResponse(rs.getLong("playerId"),
                        rs.getString("playerName"), rs.getLong("totalShotScore")));
    }

    public List<PlayerPointsResponse> getAllPlayerPoints(long gameId) {
        return jdbcTemplate.query("SELECT p.ID AS playerId, p.NAME AS playerName, SUM(s.SCORE) AS totalShotScore " +
                        "FROM GAME g INNER JOIN PLAYER p ON (p.GAME_ID = g.ID) " +
                        "LEFT JOIN SHOT s ON s.PLAYER_ID = p.ID " +
                        "WHERE g.ID = ? AND g.END IS NULL " +
                        "GROUP BY p.ID ",
                new Object[]{ gameId }, (rs, rowNum) -> new PlayerPointsResponse(rs.getLong("playerId"),
                        rs.getString("playerName"), rs.getLong("totalShotScore")));
    }

    public List<PlayerTableEntry> getAllPlayerTableEntries(long gameId, long playerId) {
        return jdbcTemplate.query("SELECT p.ID AS playerId, p.NAME AS playerName, s.SCORE AS score, s.SHOT_NUMBER AS scoreNumber, " +
                        "s.REGISTERED AS registered, s.FRAME AS frame " +
                        "FROM GAME g INNER JOIN PLAYER p ON (p.GAME_ID = g.ID) " +
                        "LEFT JOIN SHOT s ON s.PLAYER_ID = p.ID " +
                        "WHERE g.ID = ? AND p.ID = ? AND g.END IS NULL ",
                new Object[]{ gameId, playerId }, (rs, rowNum) -> new PlayerTableEntry(rs.getLong("playerId"),
                        rs.getString("playerName"), rs.getInt("score"), rs.getInt("scoreNumber"),
                        rs.getTimestamp("registered").toLocalDateTime(), rs.getInt("frame")));
    }
}
