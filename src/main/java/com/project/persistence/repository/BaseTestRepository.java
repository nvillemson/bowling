package com.project.persistence.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Service
public class BaseTestRepository {

    private EntityManager entityManager;

    @Autowired
    public BaseTestRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    @Transactional
    public void clearDatabase() {
        entityManager.flush();
        entityManager.createNativeQuery("SET REFERENTIAL_INTEGRITY FALSE").executeUpdate();
        entityManager.createNativeQuery("TRUNCATE TABLE PLAYER").executeUpdate();
        entityManager.createNativeQuery("TRUNCATE TABLE GAME").executeUpdate();
        entityManager.createNativeQuery("TRUNCATE TABLE SHOT").executeUpdate();
        entityManager.createNativeQuery("SET REFERENTIAL_INTEGRITY TRUE").executeUpdate();
        entityManager.flush();
    }

}
