package com.project.web.service;

import com.project.persistence.repository.GameRepository;
import com.project.persistence.repository.GameStatisticsRepository;
import com.project.web.handler.exceptions.GameNotFoundException;
import com.project.web.pojo.response.AllPlayerPointsResponse;
import com.project.web.pojo.response.PlayerPointsResponse;
import com.project.web.pojo.response.PlayerTableResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameStatisticsService {

    private GameRepository gameRepository;
    private GameStatisticsRepository gameStatisticsRepository;

    @Autowired
    public GameStatisticsService(GameRepository gameRepository, GameStatisticsRepository gameStatisticsRepository) {
        this.gameRepository = gameRepository;
        this.gameStatisticsRepository = gameStatisticsRepository;
    }

    public PlayerPointsResponse getPlayerPoints(long gameId, long playerId) {
        checkPlayerExists(gameId, playerId);
        return gameStatisticsRepository.getPlayerPoints(gameId, playerId);
    }

    public AllPlayerPointsResponse getAllPlayerPoints(long gameId) {
        checkGameExists(gameId);
        return new AllPlayerPointsResponse(gameStatisticsRepository.getAllPlayerPoints(gameId));
    }

    public PlayerTableResponse getPlayerTable(long gameId, long playerId) {
        checkPlayerExists(gameId, playerId);
        return new PlayerTableResponse(gameStatisticsRepository.getAllPlayerTableEntries(gameId, playerId));
    }

    private void checkGameExists(long gameId) {
        if (!gameRepository.checkGameExists(gameId)) {
            throw new GameNotFoundException("Game with id " + gameId + "not found.");
        }
    }

    private void checkPlayerExists(long gameId, long playerId) {
        if (!gameRepository.checkPlayerRegisteredForGame(gameId, playerId)) {
            throw new GameNotFoundException("Game with id " + gameId + "not found. Or player with id " + playerId
                    + " not registered for this game.");
        }
    }
}
