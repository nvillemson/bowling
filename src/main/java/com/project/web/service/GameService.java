package com.project.web.service;

import com.project.persistence.repository.GameRepository;
import com.project.web.handler.exceptions.GameNotFoundException;
import com.project.web.handler.exceptions.NoShotsAvailableExeption;
import com.project.web.handler.exceptions.ScoreTooBigException;
import com.project.web.pojo.helper.FrameResult;
import com.project.web.pojo.request.AddGameRequest;
import com.project.web.pojo.request.AddPlayerRequest;
import com.project.web.pojo.request.MakeShotRequest;
import com.project.web.pojo.response.GameStateResponse;
import com.project.web.pojo.response.IdResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class GameService {

    private GameRepository gameRepository;
    private static final int MAX_TURNS_PER_FRAME = 3;
    private static final int MAX_FRAME = 10;
    private static final int STRIKE = 10;

    @Autowired
    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public IdResponse addGame(AddGameRequest req) {
        return new IdResponse(gameRepository.addGame(req.getGameName()));
    }


    public IdResponse addPlayer(AddPlayerRequest req) {
        if (!gameRepository.checkGameExistsAndNotStarted(req.getGameId())) {
            throw new GameNotFoundException("Game with id " + req.getGameId() + "not found.");
        }
        return new IdResponse(gameRepository.addPlayer(req.getPlayerName(), req.getGameId()));
    }

    public void startGame(long gameId) {
        if (!gameRepository.checkGameExistsAndNotStarted(gameId)) {
            throw new GameNotFoundException("Game with id " + gameId + "not found.");
        }
        gameRepository.startGame(gameId);
    }

    public GameStateResponse getPlayerTurn(long gameId) {
        if (!gameRepository.checkGameExistsAndNotEnded(gameId)) {
            throw new GameNotFoundException("Game with id " + gameId + "not found.");
        }

        int currentFrame = 1;
        try {
            currentFrame = gameRepository.getCurrentFrame(gameId);
        } catch (EmptyResultDataAccessException ex) {
            // just no current frames found
        }

        List<FrameResult> frameResults = gameRepository.getFrameResults(gameId, currentFrame);
        Set<Long> playerIds = new TreeSet<>();
        frameResults.forEach(result -> playerIds.add(result.getPlayerId()));
        Long playerIdTurn = getCurrentPlayer(playerIds, frameResults);

        // end game
        if (playerIdTurn == null && currentFrame == MAX_FRAME) {
            gameRepository.endGame(gameId);
            return new GameStateResponse(LocalDateTime.now());
        }

        // get first player, in case frame end or first frame
        if (playerIdTurn == null) {
            List<Long> playerIdsList = new ArrayList<>(playerIds);
            currentFrame = currentFrame + 1;
            return new GameStateResponse(currentFrame, playerIdsList.get(0));
        } else {
            return new GameStateResponse(currentFrame, playerIdTurn);
        }
    }

    public void makeShot(MakeShotRequest req) {
        if (!gameRepository.checkPlayerRegisteredForGame(req.getGameId(), req.getPlayerId())) {
            throw new GameNotFoundException("Game with id " + req.getGameId() + "not found. Or player with id " + req.getPlayerId() + " not registered for this game.");
        }

        if (req.getScore() > STRIKE) {
            throw new ScoreTooBigException("Score " + req.getScore() + " is too big.");
        }

        int shotNumber = 1;
        List<FrameResult> frameResultsForPlayer = gameRepository.getFrameResultsForPlayer(req.getGameId(), req.getFrame(), req.getPlayerId());
        if (!frameResultsForPlayer.isEmpty()) {
            shotNumber = validateExistingShots(req, frameResultsForPlayer);
        }

        gameRepository.saveShot(req.getPlayerId(), req.getFrame(), req.getScore(), shotNumber);
    }

    private int validateExistingShots(MakeShotRequest req, List<FrameResult> frameResultsForPlayer) {
        if (!canPlayerShoot(frameResultsForPlayer)) {
            throw new NoShotsAvailableExeption("No shots available for player " + req.getPlayerId() + " and frame " + req.getFrame() + ".");
        }
        FrameResult finalResult = frameResultsForPlayer.get(frameResultsForPlayer.size() - 1);
        if (finalResult.getShotScore() != null && !finalResult.getShotScore().equals(STRIKE) && !isSpare(frameResultsForPlayer)) {
            if (finalResult.getShotScore() + req.getScore() > STRIKE) {
                throw new ScoreTooBigException("Score " + req.getScore() + " is too big.");
            }
        }
        return finalResult.getShotNumber() == null ? 1 : finalResult.getShotNumber() + 1;
    }

    private Long getCurrentPlayer(Set<Long> playerIds, List<FrameResult> frameResults) {
        Map<Long, List<FrameResult>> resultsByPlayer = new HashMap<>();
        frameResults.forEach(result -> {
            populateResultsById(resultsByPlayer, result);
        });
        for (Long playerId : playerIds) {
            List<FrameResult> playerFrameResults = resultsByPlayer.get(playerId);
            if (canPlayerShoot(playerFrameResults)) {
                return playerId;
            }
        }
        return null;
    }

    private void populateResultsById(Map<Long, List<FrameResult>> resultsByPlayer, FrameResult result) {
        List<FrameResult> results = resultsByPlayer.get(result.getPlayerId());
        if (results == null) {
            results = new ArrayList<>();
        }
        if (result.getShotNumber() != 0) {
            results.add(result);
        }
        resultsByPlayer.put(result.getPlayerId(), results);
    }

    private boolean canPlayerShoot(List<FrameResult> playerFrameResults) {
        // maximum shots - false
        if (playerFrameResults.size() == MAX_TURNS_PER_FRAME) {
            return false;
        }
        // first is strike then always 3 shots
        if (checkStrikeAsFirstResult(playerFrameResults)) return true;
        // first 2 are spare, then 3 shots
        if (isSpare(playerFrameResults)) return true;
        // 0 or 1 shot made
        return playerFrameResults.size() == 1 || playerFrameResults.isEmpty();
    }

    private boolean checkStrikeAsFirstResult(List<FrameResult> playerFrameResults) {
        if (playerFrameResults.size() >= 1) {
            Integer shotScore = playerFrameResults.get(0).getShotScore();
            return shotScore != null && shotScore == STRIKE;
        }
        return false;
    }

    private boolean isSpare(List<FrameResult> playerFrameResults) {
        if (playerFrameResults.size() == 2) {
            Integer shotScore = playerFrameResults.get(0).getShotScore();
            Integer shotScore2 = playerFrameResults.get(1).getShotScore();
            return shotScore + shotScore2 == STRIKE;
        }
        return false;
    }
}
