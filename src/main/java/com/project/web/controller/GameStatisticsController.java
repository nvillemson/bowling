package com.project.web.controller;

import com.project.web.pojo.response.AllPlayerPointsResponse;
import com.project.web.pojo.response.PlayerPointsResponse;
import com.project.web.pojo.response.PlayerTableResponse;
import com.project.web.service.GameStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@SuppressWarnings("unused")
@Controller
public class GameStatisticsController {

    private GameStatisticsService gameStatisticsService;

    @Autowired
    public GameStatisticsController(GameStatisticsService gameStatisticsService) {
        this.gameStatisticsService = gameStatisticsService;
    }

    @GetMapping("/get-player-points")
    public @ResponseBody PlayerPointsResponse getPlayerPoints(@RequestParam("gameId") long gameId, @RequestParam("playerId") long playerId) {
        return gameStatisticsService.getPlayerPoints(gameId, playerId);
    }

    @GetMapping("/get-all-points")
    public @ResponseBody AllPlayerPointsResponse getPlayerPoints(@RequestParam("gameId") long gameId) {
        return gameStatisticsService.getAllPlayerPoints(gameId);
    }

    @GetMapping("/get-player-table")
    public @ResponseBody PlayerTableResponse getPlayerTable(@RequestParam("gameId") long gameId, @RequestParam("playerId") long playerId) {
        return gameStatisticsService.getPlayerTable(gameId, playerId);
    }

}
