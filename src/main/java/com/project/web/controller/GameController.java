package com.project.web.controller;

import com.project.web.pojo.request.AddGameRequest;
import com.project.web.pojo.request.AddPlayerRequest;
import com.project.web.pojo.request.MakeShotRequest;
import com.project.web.pojo.response.GameStateResponse;
import com.project.web.pojo.response.IdResponse;
import com.project.web.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@SuppressWarnings("unused")
@Controller
public class GameController {

    private GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping("/add-game")
    public @ResponseBody IdResponse addGame(@RequestBody @Valid AddGameRequest addGameRequest) {
        return gameService.addGame(addGameRequest);
    }

    @PostMapping("/add-player")
    public @ResponseBody IdResponse addGame(@RequestBody @Valid AddPlayerRequest addPlayerRequest) {
        return gameService.addPlayer(addPlayerRequest);
    }

    @PostMapping("/start-game")
    @ResponseStatus(HttpStatus.OK)
    public void startGame(@RequestParam("gameId") long gameId) {
        gameService.startGame(gameId);
    }

    @GetMapping("/get-player-turn")
    public @ResponseBody GameStateResponse getPlayerTurn(@RequestParam("gameId") long gameId) {
        return gameService.getPlayerTurn(gameId);
    }

    @PostMapping("/make-shot")
    @ResponseStatus(HttpStatus.OK)
    public void makeShot(@RequestBody @Valid MakeShotRequest makeShotRequest) {
        gameService.makeShot(makeShotRequest);
    }


}
