package com.project.web.pojo.response;

import com.project.web.pojo.helper.PlayerTableEntry;

import java.util.List;

public class PlayerTableResponse {

    private List<PlayerTableEntry> playerShots;

    public PlayerTableResponse(List<PlayerTableEntry> playerShots) {
        this.playerShots = playerShots;
    }

    public PlayerTableResponse() {
    }

    public List<PlayerTableEntry> getPlayerShots() {
        return playerShots;
    }

    public void setPlayerShots(List<PlayerTableEntry> playerShots) {
        this.playerShots = playerShots;
    }
}
