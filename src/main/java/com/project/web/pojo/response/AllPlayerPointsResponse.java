package com.project.web.pojo.response;

import java.util.List;

public class AllPlayerPointsResponse {

    private List<PlayerPointsResponse> response;

    public AllPlayerPointsResponse(List<PlayerPointsResponse> response) {
        this.response = response;
    }

    public AllPlayerPointsResponse() {
    }

    public List<PlayerPointsResponse> getResponse() {
        return response;
    }

    public void setResponse(List<PlayerPointsResponse> response) {
        this.response = response;
    }
}
