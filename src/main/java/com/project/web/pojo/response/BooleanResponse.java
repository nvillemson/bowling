package com.project.web.pojo.response;

public class BooleanResponse {

    private boolean response;

    public BooleanResponse(boolean response) {
        this.response = response;
    }

    public BooleanResponse() {
    }

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }
}
