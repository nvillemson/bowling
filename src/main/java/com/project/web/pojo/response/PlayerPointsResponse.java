package com.project.web.pojo.response;

public class PlayerPointsResponse {

    private long playerId;
    private String playerName;
    private long playerPoints;

    public PlayerPointsResponse(long playerId, String playerName, long playerPoints) {
        this.playerId = playerId;
        this.playerName = playerName;
        this.playerPoints = playerPoints;
    }

    public PlayerPointsResponse() {
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public long getPlayerPoints() {
        return playerPoints;
    }

    public void setPlayerPoints(long playerPoints) {
        this.playerPoints = playerPoints;
    }
}
