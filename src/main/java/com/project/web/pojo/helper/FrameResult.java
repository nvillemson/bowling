package com.project.web.pojo.helper;

public class FrameResult {

    private Long playerId;
    private Integer shotNumber;
    private Integer shotScore;

    public FrameResult(Long playerId, Integer shotNumber, Integer shotScore) {
        this.playerId = playerId;
        this.shotNumber = shotNumber;
        this.shotScore = shotScore;
    }

    public FrameResult() {
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public Integer getShotNumber() {
        return shotNumber;
    }

    public void setShotNumber(Integer shotNumber) {
        this.shotNumber = shotNumber;
    }

    public Integer getShotScore() {
        return shotScore;
    }

    public void setShotScore(Integer shotScore) {
        this.shotScore = shotScore;
    }
}
