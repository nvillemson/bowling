package com.project.web.pojo.request;

import javax.validation.constraints.NotNull;

public class AddPlayerRequest {

    @NotNull
    private String playerName;

    @NotNull
    private Long gameId;

    public AddPlayerRequest(@NotNull String playerName, @NotNull Long gameId) {
        this.playerName = playerName;
        this.gameId = gameId;
    }

    public AddPlayerRequest() {
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }
}
