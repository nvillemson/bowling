package com.project.web.pojo.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class MakeShotRequest {

    @NotNull
    private Long gameId;

    @NotNull
    private Long playerId;

    @NotNull
    private Integer score;

    @NotNull
    @Max(10)
    @Min(1)
    private Integer frame;

    public MakeShotRequest(@NotNull Long gameId, @NotNull Long playerId, @NotNull Integer score, @NotNull Integer frame) {
        this.gameId = gameId;
        this.playerId = playerId;
        this.score = score;
        this.frame = frame;
    }

    public MakeShotRequest() {
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getFrame() {
        return frame;
    }

    public void setFrame(Integer frame) {
        this.frame = frame;
    }
}
