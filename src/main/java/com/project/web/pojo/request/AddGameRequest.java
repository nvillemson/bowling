package com.project.web.pojo.request;

import javax.validation.constraints.NotNull;

public class AddGameRequest {

    @NotNull
    private String gameName;

    public AddGameRequest(@NotNull String gameName) {
        this.gameName = gameName;
    }

    public AddGameRequest() {
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }
}
