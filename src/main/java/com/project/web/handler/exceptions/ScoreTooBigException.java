package com.project.web.handler.exceptions;

public class ScoreTooBigException extends RuntimeException {
    public ScoreTooBigException(String message) {
        super(message);
    }
}
