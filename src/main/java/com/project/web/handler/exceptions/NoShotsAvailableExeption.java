package com.project.web.handler.exceptions;

public class NoShotsAvailableExeption extends RuntimeException {
    public NoShotsAvailableExeption(String message) {
        super(message);
    }
}
