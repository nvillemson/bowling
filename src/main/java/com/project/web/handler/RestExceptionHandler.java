package com.project.web.handler;

import com.project.web.handler.exceptions.GameNotFoundException;
import com.project.web.handler.exceptions.NoShotsAvailableExeption;
import com.project.web.handler.exceptions.ScoreTooBigException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@SuppressWarnings("unused")
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(GameNotFoundException.class)
    protected ResponseEntity<String> handleGameNotFound(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }

    @ExceptionHandler(NoShotsAvailableExeption.class)
    protected ResponseEntity<String> handleNoShotsAvailable(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }

    @ExceptionHandler(ScoreTooBigException.class)
    protected ResponseEntity<String> handleScoreTooBig(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }

}
